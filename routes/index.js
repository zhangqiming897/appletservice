const express = require("express");
const router = express.Router();
const sequelize = require("../utils/index");
const validateToken = require("../utils/validateToken");
const { textMessage } = require("../utils/chatMessage");
const { Configuration, OpenAIApi } = require("openai");
const configuration = new Configuration({
    organization: 'org-10jmswYMegiwKDjfUOMuasrF',
    apiKey: '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'
});
const openai = new OpenAIApi(configuration);

/* 服务 */

// 测试连接是否正常
sequelize
  .authenticate()
  .then(() => {
    console.log("数据库连接成功!");
  })
  .catch((error) => {
    console.error("数据库连接失败", error);
  });

router.get("/", (req, res) => {
  validateToken(req).then((t) => {
    res.send(t);
  });
});

router.post("/", (req, res) => {
  console.log('req.body',req.body)
  let xml = req.body.xml;
  let msgtype = xml.msgtype[0];
  let content = xml.content[0];
  const message = (reply) => {
    return {
      FromUserName: xml.fromusername[0],
      ToUserName: xml.tousername[0],
      reply: reply
    }
  };
  switch (msgtype) {
    case "text":
      openai.createCompletion({
            model: 'text-davinci-003',
            prompt: content,
            temperature: 0,
            max_tokens: 2048
      }).then(response => {
        let { data: { choices } } = response;
        console.log('choices', choices);
        // 封装要回复的消息参数
        let replyMessage = choices[0].text.replace(/(\n)+/g, '我的回答: ');
        res.send(textMessage(message(replyMessage)));
      }).catch(error => {
        res.send(textMessage(message('未能识别, 请重新回答!')));
      })
      break;
    default:
      res.send("");
      break;
  }
});

module.exports = router;
