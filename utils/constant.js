/**
 * 描述: 自定义常量
 */

module.exports = {
    CODE_ERROR: -1,
    CODE_SUCCESS: 0,
    CODE_TOKEN_EXPIRED: 401,
    PRIVATE_KEY: 'appservice',
    JWT_EXPIRED: 60 * 60 * 24
}