/**
 * 共用方法
 */

// 产生6位随机数(用来生成短信验证码)
const getCode = () => {
    let str = '';
    new Array(6).fill(1).forEach(ele => {
        str += parseInt(Math.random() * 10)
    })
    return str;
}

module.exports = {
    getCode
}