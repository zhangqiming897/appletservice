/**
 * 描述: jwt-token验证和解析函数
 */
const jwt = require('jsonwebtoken');
const expressJwt = require('express-jwt');
const { PRIVATE_KEY } = require('./constant');

// 
const jwtAuth = expressJwt({
    //
    secret: PRIVATE_KEY,
    credentialsRequired: true,
    algorithms: ['HS256'],
    getToken: (req) => {
        if(req.headers.authorization){
            return req.headers.authorization
        }
        if(req.query && req.query.token){
            return req.query.token
        }
    }
}).unless({
    path: [
        '/',
        'rest/user/login'
    ]
});

// jwt-token
const decode = (req) => {
    const token = req.get('Authorization');
    return jwt.verify(token, PRIVATE_KEY);
}

module.exports = {
    jwtAuth,
    decode
}