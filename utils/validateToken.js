const sha1 = require('sha1');

const config = {
    wechat:{
      appid: 'wx32eb6ad62a6ac78b',
      appsecret: '0b1e95240ee3c8e6c151158d0b46dbfb',
      token: 'wAky5xio'
    }
  }

const validateToken = (req) => {
    return new Promise((resolve, reject) => {
        let token = config.wechat.token;
        let signature = req.query.signature;
        let nonce = req.query.nonce;
        let timestamp = req.query.timestamp;
        let echostr = req.query.echostr;
        let str = [token, timestamp, nonce].sort().join('');
        let sha = sha1(str);
        if(sha === signature) {
            resolve(echostr);
        } else {
            reject(false)
        }
    })
}

// 导出验证 Token 发放
module.exports = validateToken;