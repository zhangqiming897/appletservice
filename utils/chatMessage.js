/**
 * 微信公众号回复
 */

// 回复文本消息
const textMessage = (message) => {
    let createTime = new Date().getTime();
    return `<xml>
    <ToUserName><![CDATA[${message.FromUserName}]]></ToUserName>
    <FromUserName><![CDATA[${message.ToUserName}]]></FromUserName>
    <CreateTime>${createTime}</CreateTime>
    <MsgType><![CDATA[text]]></MsgType>
    <Content><![CDATA[${message.reply}]]></Content>
    </xml>`;
}

module.exports = {
    textMessage
}
