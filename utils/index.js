/**
 * 描述: mysql模块连接
 */
const config = require('../config/config');
const Sequelize = require('sequelize');

// 
const sequelize = new Sequelize(
    config.database,
    config.user,
    config.password,
    {
        dialect: 'mysql',
        host: config.host,
        port: config.port
    }
)


module.exports = sequelize;