/**
 * 描述: md5密码加密
 */
const crypto = require('crypto');

const md5 = (s) => {
    return crypto.createHash('md5').update(s).digest('hex');
}

module.exports = md5;