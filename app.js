/**
 * 描述: 入口文件
 */
const bodyParser = require('body-parser');
const express = require('express');
const cors = require('cors');
const routes = require('./routes');
const app = express();

const xmlparser = require('express-xml-bodyparser'); // 解析 xml



app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended:true }));

app.use(cors());
app.use(xmlparser());

app.use(routes);

app.listen(80)