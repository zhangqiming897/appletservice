/**
 * 描述:  业务逻辑处理
 */

const UserModel = require('./../model/user');
const md5 = require('../utils/md5');
const jwt = require('jsonwebtoken');
const boom = require('boom');
const { validationResult } = require('express-validator');
const {
    CODE_ERROR,
    CODE_SUCCESS,
    PRIVATE_KEY,
    JWT_EXPIRED
} = require('../utils/constant');

// 引入阿里云短信服务模块
const SMSClient = require('@alicloud/sms-sdk');
const { getCode } = require('./../utils/func');

/**
 * 账号登录
 */
const login = (req, res, next) => {
    const err = validationResult(req);
    if(!err.isEmpty()) {
        const [{ msg }] = err.errors;
        next(boom.badRequest(msg));
    } else {
        let { account, password } = req.body;
        // md5 加密
        password = md5(password);
        UserModel.findAll({
            where: {
                account,
                password
            }
        }).then(data => {
            data.length || res.json({
                code: CODE_ERROR,
                msg: '账号/密码信息有误，请重新输入!',
                data: null
            });
            data.length && (() => {
                switch(data[0].active) {
                    case 1: 
                    // 登录成功, 签发一个token返回给前端
                    const token = jwt.sign(
                        // 签发token里包含的一些数据
                        { account },
                        // 私钥
                        PRIVATE_KEY,
                        // 设置过期时间
                        { expiresIn: JWT_EXPIRED }
                    );
                    res.json({
                        code: CODE_SUCCESS,
                        msg: '登录成功',
                        data: {
                            token,
                            result: data
                        }
                    });
                    break;
                    default:
                        res.json({
                            code: CODE_ERROR,
                            msg: '该账号已被禁用',
                            data: null
                        });
                    break;
                }
            })()
        }).catch(error => {
           res.json({
             code:CODE_ERROR,
             msg: '接口已被停用',
             data: error
           })
        })
    }
}

/**
 * 短信验证登录
 */
const messageVerify = (req, res, next) => {
    const err = validationResult(req);
    if(!err.isEmpty()) {
        const [{ msg }] = err.errors;
        next(boom.badRequest(msg));
    } else {
        let { phoneNum } = req.body;
        
        // 初始化 sms_client
        let smsClient = new SMSClient({
            accessKeyId: '*********', // accessKeyId
            secretAccessKey: '********', // secretAccessKey
        });

        // 6位随机数
        let str = getCode();

        // 开始发送短信
        smsClient.sendSMS({
            PhoneNumbers: phoneNum,
            SignName: '*******',
            TemplateCode: '*******',
            TemplateParam: `{'code': '${str}'}`,
        }).then(result => {
            let { Code } = result;
            if(Code == 'OK') {
                res.json({
                    code: 0,
                    msg: 'success',
                    sms: str
                }).catch(err => {
                    res.json({
                        code: 1,
                        msg: 'fail:' + err.data.Message
                    })
                })
            }
        })
    }
}


module.exports = {
    login,
    messageVerify
}