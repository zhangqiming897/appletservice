/**
 * 描述:  业务逻辑处理
 */

const SysModel = require('./../model/sys');
const boom = require('boom');
const { validationResult } = require('express-validator');
const {
    CODE_ERROR,
    CODE_SUCCESS
} = require('../utils/constant'); 

/**
 * 版本管理
 */
const queryVersion = (req, res, next) => {
    const err = validationResult(req);
    if(!err.isEmpty()) {
        const [{ msg }] = err.errors;
        next(boom.badRequest(msg));
    } else {
        SysModel.findAll().then(data => {
            data.length || res.json({
                code: CODE_ERROR,
                msg: '未能获取到版本号信息!',
                data: null
            });
            data.length && res.json({
                code: CODE_SUCCESS,
                msg: '获取到版本号信息!',
                data
            })
        }).catch(error => {
            res.json({
              code: CODE_ERROR,
              msg: '接口已被停用',
              data: error
            })
        })
    }
}

module.exports = {
    queryVersion
}