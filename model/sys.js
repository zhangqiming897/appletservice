const sequelize = require("../utils/index");
const { DataTypes } = require('sequelize');

/* 定义系统类型 */

const Sys = sequelize.define("sys", {
    sysId: DataTypes.INTEGER,
    version: DataTypes.STRING
});

module.exports = Sys;