const sequelize = require("../utils/index");
const { DataTypes } = require('sequelize');

/* 定义用户模型 */

const User = sequelize.define("user", {
    userId: DataTypes.INTEGER,
    phone: DataTypes.STRING,
    account: DataTypes.STRING,
    password: DataTypes.STRING,
    createTime: DataTypes.DATE,
    active: DataTypes.BOOLEAN
});

module.exports = User;